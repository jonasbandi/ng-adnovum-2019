import {Component, EventEmitter, Output} from '@angular/core';
import {ToDo} from '../../model/todo.model';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

@Component({
  selector: 'td-new-todo',
  templateUrl: './new-todo.component.html',
})
export class NewTodoComponent {

  theForm: FormGroup;
  @Output() addToDo = new EventEmitter<ToDo>();

  constructor(formBuilder: FormBuilder) {
    this.theForm = formBuilder.group({
      newToDoTitle: ['', [Validators.required, Validators.minLength(3)]]
      // newToDoTitle: ['', [Validators.required, Validators.minLength(3), validateTodo]]
    });

    // programmatic changes of validation
    // setTimeout(this.enableValidation.bind(this), 5000);
  }

  onSubmit() {
    this.addToDo.emit(new ToDo(this.theForm.value.newToDoTitle)); // accessing the value of the form
    // this.addToDo.emit(new ToDo(this.theForm.get('newToDoTitle').value)); // Alternative: accessing the value of the control
    // this.addToDo.emit(new ToDo(this.theForm.controls.newToDoTitle.value)); // Alternative: accessing the value of the control
    // this.theForm.reset();
    // this.theForm.controls.newToDoTitle.setValue('') // Alternative: setting the value of the control
    this.theForm.patchValue({newToDoTitle: ''}); // Alternative: setting the value of the control
  }

  enableValidation() {
    console.log('changing validators...');
    const titleControl = this.theForm.get('newToDoTitle');
    titleControl.setValidators([
      Validators.required,
      Validators.minLength(3),
      validateTodo
    ]);
    titleControl.updateValueAndValidity();
  }
}

const validateTodo: ValidatorFn =  (control) => {

  const firstChar = control.value && control.value.charAt(0);

  if (!firstChar || firstChar === firstChar.toUpperCase()) {
    return null;
  } else {
    return { capitalLetter: { valid: false } };
  }
}
