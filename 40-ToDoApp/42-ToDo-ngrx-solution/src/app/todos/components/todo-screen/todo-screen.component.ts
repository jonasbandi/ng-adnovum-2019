import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ToDo } from '../../model/todos/todo.model';
import { IToDosState } from '../../model/todos/todo.store';
import { Store, createSelector, createFeatureSelector } from '@ngrx/store';
import { getPendingToDos, getDoneToDos } from '../../model/todos/todo.selectors';
import { completeRequest, createRequest, loadRequest } from '../../model/todos/todo.actions';

@Component({
  selector: 'td-overview',
  template: `
      <h4>Let's do some work:</h4>

      <div class="link-container">
          <a routerLink="/done">
              Done: {{(doneTodos$ | async)?.length}}
          </a>
      </div>

      <td-new-todo (addToDo)="addToDo($event)"></td-new-todo>

      <section class="main">
          <td-todo-list [todos]="pendingTodos$ | async" (removeToDo)="completeToDo($event)"></td-todo-list>
      </section>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoScreenComponent implements OnInit {

  pendingTodos$: Observable<ToDo[]>;
  doneTodos$: Observable<ToDo[]>;

  constructor(private store: Store<{ todos: IToDosState }>) {
    this.pendingTodos$ = this.store.select(getPendingToDos);
    this.doneTodos$ = this.store.select(getDoneToDos);
  }

  ngOnInit(): void {
    console.log('Loading TODOS');
    this.store.dispatch(loadRequest());
  }

  addToDo(todo: ToDo) {
    this.store.dispatch(createRequest({todo}));
  }

  completeToDo(todo: ToDo) {
    this.store.dispatch(completeRequest({todo}));
  }

}
