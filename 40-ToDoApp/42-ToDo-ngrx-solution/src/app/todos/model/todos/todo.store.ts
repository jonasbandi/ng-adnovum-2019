import { ToDo } from './todo.model';
import { completeRequest, createSuccess, loadSuccess, removeRequest } from './todo.actions';
import { Action, createReducer, on } from '@ngrx/store';

export interface IToDosState {
  pendingTodos: ToDo[];
  doneTodos: ToDo[];
}

export const initialState: IToDosState = {
  pendingTodos: [],
  doneTodos: []
};

const reducer = createReducer(
  initialState,
  on(loadSuccess, (state, {todos}) => {
    const pendingTodos = todos.filter(t => !t.completed);
    const doneTodos = todos.filter(t => t.completed);
    return {...state, pendingTodos, doneTodos};
  }),
  on(createSuccess, (state, {todo}) => {
    const pendingTodos = [...state.pendingTodos, todo];
    return {...state, pendingTodos};
  }),
  on(completeRequest, (state, {todo}) => {
    const completedToDo = new ToDo(todo.title, todo.id);
    completedToDo.completed = true;
    const pendingTodos = state.pendingTodos.filter(t => t !== todo);
    const doneTodos = [...state.doneTodos, completedToDo];
    return {...state, pendingTodos, doneTodos};
  }),
  on(removeRequest, (state, {todo}) => {
    const doneTodos = state.doneTodos.filter(t => t !== todo);
    return {...state, doneTodos};
  }),
);

export function todoReducer(state: IToDosState | undefined, action: Action) {
  return reducer(state, action);
}

// DEMO:
// Mutate the state and show ngrx-store-freeze throws an exception:
// i.e in on(createSuccess)
//    state.pendingTodos.push(todo);
//    return state;
