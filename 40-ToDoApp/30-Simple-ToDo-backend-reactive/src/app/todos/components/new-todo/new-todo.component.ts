import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import {ToDo} from '../../model/todo.model';

@Component({
  selector: 'td-new-todo',
  template: `
      <div class="new-todo">
          <form #formRef="ngForm" (ngSubmit)=" formRef.valid && onAddToDo()">
              <input #inputRef="ngModel"
                     name="newToDoTitle"
                     [(ngModel)]="newToDoTitle"
                     required minlength="3"
                     type="text"
                     placeholder="What needs to be done???"
                     autofocus autocomplete="off"/>
              <div class="add-button-container">
                  <button *ngIf="!saving && formRef.valid" type="submit" class="add-button" id="add-button">
                      +
                  </button>
                  <div *ngIf="saving" class="remove-button">
                      <td-spinner [spinnerSize]="'30px'" [delay]="0"></td-spinner>
                  </div>
              </div>
          </form>
      </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewTodoComponent {

  newToDoTitle = '';
  @Input() saving: boolean;
  @Output() addToDo = new EventEmitter<ToDo>();

  onAddToDo(): void {
    this.addToDo.emit(new ToDo(this.newToDoTitle));
    this.newToDoTitle = '';
  }

}
