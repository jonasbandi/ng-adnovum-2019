import { Injectable } from '@angular/core';
import { merge, Observable, Subject } from 'rxjs';
import { map, scan, switchMap, } from 'rxjs/operators'; // pipeable operators are new since RxJS 5.5
import { ToDo } from './todo.model';
import { ToDoApiService } from './todo-api.service';

// Explicitely model the state changes
enum TODO_ACTION {
  LOAD = 'LOAD',
  ADD = 'ADD',
  COMPLETE = 'COMPLETE',
  REMOVE = 'REMOVE',
}

type ToDoAction = {
  type: TODO_ACTION;
  payload: any; // it would be possible to type the payload ...
};

function processToDoAction(todos: ToDo[], action: ToDoAction): ToDo[] {
  console.log('TODOS BEFORE ACTION: ', todos);
  console.log('PROCESSING ACTION: ', action);
  let newState;
  if (action.type === TODO_ACTION.LOAD) {
    newState = action.payload;
  } else if (action.type === TODO_ACTION.ADD) {
    newState = [...todos, action.payload];
  } else if (action.type === TODO_ACTION.REMOVE) {
    newState = todos.filter(t => t.id !== action.payload.id);
  } else if (action.type === TODO_ACTION.COMPLETE) {
    newState = todos.map(t => t.id !== action.payload.id ? t : {...action.payload, completed: true});
  } else {
    newState = todos;
  }
  console.log('TODOS AFTER ACTION: ', newState);
  return newState;
}

@Injectable({providedIn: 'root'})
export class ToDoStateService {

  // Action Streams
  private loadedToDos = new Subject<ToDo[]>();

  private todosInsertedSubject = new Subject<ToDo>();
  todoInsertedAction$ = this.todosInsertedSubject.asObservable()
    .pipe(
      switchMap(todo => this.toDoApi.saveTodo(todo)), // pessimistic ui

    );

  private todoCompleteSubject = new Subject<ToDo>();
  todoCompletedAction$ = this.todoCompleteSubject.asObservable()
    .pipe(
      map(todo => ({...todo, completed: true})),
      switchMap(todo => this.toDoApi.updateTodo(todo).pipe(map(() => todo))), // pessimistic ui

    );

  private todoDeleteSubject = new Subject<ToDo>();
  todoDeletedAction$ = this.todoDeleteSubject.asObservable()
    .pipe(
      switchMap(todo => this.toDoApi.deleteTodo(todo).pipe(map(() => todo))), // pessimistic ui
    );

  private todos$: Observable<ToDo[]> = merge(
    this.loadedToDos.pipe(map(todos => ({type: TODO_ACTION.LOAD, payload: todos}))),
    this.todoInsertedAction$.pipe(map(todo => ({type: TODO_ACTION.ADD, payload: todo}))),
    this.todoCompletedAction$.pipe(map(todo => ({type: TODO_ACTION.COMPLETE, payload: todo}))),
    this.todoDeletedAction$.pipe(map(todo => ({type: TODO_ACTION.REMOVE, payload: todo}))),
  )
    .pipe(
      scan((acc: ToDo[], action: any) => processToDoAction(acc, action), []),
    );

  pendingToDos$ = this.todos$.pipe(map(todos => todos.filter(t => !t.completed)));
  completedToDos$ = this.todos$.pipe(map(todos => todos.filter(t => !!t.completed)));

  constructor(private toDoApi: ToDoApiService) {
  }

  loadToDos(completed?: boolean) {
    // I don't see a way to avoid the following subscription, given that we want to be able to reload the data from the API ...
    this.toDoApi.getTodos(completed).subscribe(
      todos => this.loadedToDos.next(todos)
    );
  }

  addToDo(todo: ToDo) {
    this.todosInsertedSubject.next(todo);
  }

  completeToDo(todo: ToDo) {
    this.todoCompleteSubject.next(todo);

  }

  deleteToDo(todo: ToDo) {
    this.todoDeleteSubject.next(todo);
  }
}

